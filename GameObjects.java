package com.company;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;

// kelas GameObjects turunan kelas GameFrames dan mengimplementasikan ActionListener
public class GameObjects extends GameFrames implements ActionListener{


    // Deklarasi variabel
    // player sekarang
    protected int player = 1;
    // ganti giliran
    protected boolean changeTurn = false;
    // cek menang
    protected boolean win = false;
    // judul game
    protected String title;

    // konstruktor GameObjects
    protected GameObjects(String title) {
        // konstruktor super class
        super(title);
        this.title = title;
    }

    // method setComponent
    protected void setComponent(){
        // untuk mereset komponen
        resetComponent();
        // untuk mengganti giliran
        changeTurnPlayer();
    }

    // method resetComponent
    protected void resetComponent(){
        // set player sekarang = player 1
        setPlayer(1);
        // set menang = salah
        setWin(false);
        // set ganti giliran = salah
        setChangeTurn(false);

        // perulangan untuk set tombol 1 - 9
        // gunakan tombol kosong untuk mencetak satu per satu
        for(int i = 0; i < 9; i++){
            // mengecek tombol sekarang
            switchButton(i);
            // set tombol sekarang = ""
            btnTemp.setText("");
        }
    }

    // method changeTurnPlayer
    protected void changeTurnPlayer() {
        // variabel playerTurn untuk mencetak player sekarang
        String playerTurn;

        // jika player 1 maka
        if(getPlayer() == 1)
            // variabel playerTurn = "one"
            playerTurn = "one";
            // jika tidak maka
        else
            // variabel playerTurn = "two"
            playerTurn = "two";

        // set label giliran sekarang
        lblTurn.setText("Player " + playerTurn + "s' turn");
    }

    // method eventComponent
    protected void eventComponent() {
        // perulangan untuk menambahkan Action Listener
        for(int i = 0; i < 9; i++){
            // mengecek tombol sekarang
            switchButton(i);
            // tambahkan Action Listener untuk tombol sekarang
            btnTemp.addActionListener(this);
        }

        // tambahkan Action Listener untuk menu reset
        itemReset.addActionListener(this);
        // tambahkan Action Listener untuk menu info
        itemInfo.addActionListener(this);
        // tambahkan Action Listener untuk menu exit
        itemExit.addActionListener(this);

        // Listener jika menekan exit
        frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent evt){
                // keluar dari game
                System.exit(0);
            }
        });
    }

    // method switchButton
    protected void switchButton(int index){
        switch(index){
            case 0: btnTemp = btn1; break;
            case 1: btnTemp = btn2; break;
            case 2: btnTemp = btn3; break;
            case 3: btnTemp = btn4; break;
            case 4: btnTemp = btn5; break;
            case 5: btnTemp = btn6; break;
            case 6: btnTemp = btn7; break;
            case 7: btnTemp = btn8; break;
            case 8: btnTemp = btn9; break;
        }
    }

    // get nilai player
    protected int getPlayer() {
        return player;
    }

    // set nilai player
    protected void setPlayer(int player) {
        this.player = player;
    }

    // ganti giliran
    protected boolean isChangeTurn() {
        return changeTurn;
    }

    // set ganti giliran
    protected void setChangeTurn(boolean changeTurn) {
        this.changeTurn = changeTurn;
    }

    // menang
    protected boolean isWin(){
        return win;
    }

    // set menang
    protected void setWin(boolean win){
        this.win = win;
    }

    // set menang
    protected void setWin(JButton btn1, JButton btn2, JButton btn3){
        // jika btn1 = btn2 = btn3
        // dan tidak kosong atau "" maka
        if(btn1.getText().equals(btn2.getText()) &&
                btn2.getText().equals(btn3.getText()) &&
                !btn1.getText().equals("")){
            // menang = true
            win = true;
        }
    }

    // method isNoMoreRoom
    protected boolean isNoMoreRoom(){
        // variabel untuk mengecek tombol jika sudah penuh
        boolean cek = true;

        // perulangan mengecek tombol 1 - 9
        for(int i = 0;i < 9; i++){
            switchButton(i);
            // jika tombol sekarang kosong maka
            if(btnTemp.getText().equals("")){
                // cek = false/salah
                cek = false;
            }
        }

        // kembalikan nilai cek
        return cek;
    }

    // method checkStatus
    protected void checkStatus(){
        // cek tombol mendatar
        checkHorizontal();
        // cek tombol lurus
        checkVertical();
        // cek tombol miring
        checkDiagonal();

        // jika menang = true
        // atau tombol sudah penuh
        if(isWin() || isNoMoreRoom()){
            // variabel playerWin untuk pemenang
            String playerWin;

            // jika ganti giliran = true maka
            if(isChangeTurn())
                // player menang = "one"
                playerWin = "one";
                // jika tidak maka
            else
                // player menang = "two"
                playerWin = "two";

            // tampilkan pemenang
            JOptionPane.showMessageDialog(null, "player " + playerWin + "s' win");
            // set komponen
            setComponent();
        }
    }

    // method checkHorizontal
    // mengecek tombol mendatar
    protected void checkHorizontal() {
        // cek tombol 1, 2, 3
        setWin(btn1, btn2, btn3);
        // cek tombol 4, 5, 6
        setWin(btn4, btn5, btn6);
        // cek tombol 7, 8, 9
        setWin(btn7, btn8, btn9);
    }

    // method checkVertical
    // mengecek tombol tegak
    protected void checkVertical() {
        // cek tombol 1, 4, 7
        setWin(btn1, btn4, btn7);
        // cek tombol 2, 5, 8
        setWin(btn2, btn5, btn8);
        // cek tombol 3, 6, 9
        setWin(btn3, btn6, btn9);
    }

    // method checkDiagonal
    // mengecek tombol miring
    protected void checkDiagonal() {
        // cek tombol 1, 5, 9
        setWin(btn1, btn5, btn9);
        // cek tombol 3, 5, 7
        setWin(btn3, btn5, btn7);
    }

    // method actionPerformed
    @Override
    public void actionPerformed(ActionEvent evt) {
        // mengecek tombol yang ditekan
        for(int i = 0; i < 9; i++){
            switchButton(i);
            // jika tombol yang ditekan sama dengan tombol sekarang maka
            if(evt.getSource() == btnTemp){
                // jika tombol sekarang belum digunakan maka
                if(btnTemp.getText() == ""){
                    // jika player 1 dan belum ganti giliran maka
                    if(getPlayer() == 1 && !isChangeTurn()){
                        // set Tombol sekarang dengan X
                        btnTemp.setText("X");
                        // set player = 2
                        setPlayer(2);
                        // ganti giliran
                        setChangeTurn(true);
                        // panggil method ganti giliran
                        changeTurnPlayer();
                        // jika tidak maka atau giliran player 2
                    } else {
                        // set Tombol sekarang dengan O
                        btnTemp.setText("O");
                        // set player = 1
                        setPlayer(1);
                        // ganti giliran
                        setChangeTurn(false);
                        // panggil method ganti giliran
                        changeTurnPlayer();
                    }
                }
                // panggil method checkStatus sekarang
                checkStatus();
            }
        }

        // jika menu reset maka
        if(evt.getSource() == itemReset){
            // panggil method setComponent
            // atau mereset kembali komponen
            setComponent();
        }

        // jika menu info maka tampilkan info game
        if(evt.getSource() == itemInfo){
            JOptionPane.showMessageDialog(null, "Dibuat oleh : Arif Setia Aji\n"+
                    "----------------------------------------------\n" +
                    "1900018412", title, JOptionPane.INFORMATION_MESSAGE);
        }

        // jika menu exit maka
        if(evt.getSource() == itemExit){
            // keluar dari game
            System.exit(0);
        }
    }
}